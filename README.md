# Review Apps with Nomad

This example demonstrates how to use the [Review Apps](https://docs.gitlab.com/ee/ci/review_apps/) GitLab feature with [HashiCorp Nomad](https://www.nomadproject.io/). [Scripts](./.gitlab-ci.yml) are executed in container images in GitLab CI/CD.

### GitLab CI/CD is Used to:

1. Build an image with app's build environment and push it to a container registry.
1. Build an app ([MkDocs](https://www.mkdocs.org/) static site) in its prepared environment.
1. Build an image that serves the app, and push it to a container registry.
1. Deploy an app into Nomad.

This demo is not connected to a real Nomad cluster, so you can only view job logs and the mainstream version of the app in [GitLab Pages](https://himura.gitlab.io/review-apps-nomad/). Feel free to fork it and use with your cluster.


### You'll Need:

1. A Nomad cluster accessible for project's GitLab CI/CD Runners.
1. A domain name with a [wildcard DNS record](https://en.wikipedia.org/wiki/Wildcard_DNS_record) for your review apps: we use the `<branch>.<project>.example.com` URLs, so you need to have a `*.example.com` domain pointed to your load balancer(s).
1. Some load balancing infrastructure. We suggest the following setup:
    1. A [Consul](https://www.consul.io/) cluster for service discovery and health checks.
    1. A [fabio](https://learn.hashicorp.com/tutorials/nomad/load-balancing-fabio) service job on every Nomad client (fabio is connected to Consul).
    1. A wildcard DNS record points to all Nomad clients (several A records), so that you can use load balancer on every Nomad client.
    1. [fabio](https://fabiolb.net) listens to Consul Catalog events, and automatically adds new services to the routing table. All we need to have a service available on its desired domain name is to add a [urlprefix](https://fabiolb.net/quickstart/) tag to [nomad job spec](./app.nomad#L27).
1. A container registry. This demo uses the [GitLab container registry](https://docs.gitlab.com/ee/user/packages/container_registry/), but [you can use](#how-to-use-an-external-container-registry) any other registry accessible for GitLab Runners.
1. A [Scheduled Pipeline](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) to update app's build environment image. There's only one job reacting to schedules in this demo, so you can create a schedule without variables. 
   > WARNING  
   > Push-triggered pipelines will fail with the `manifest unknown` error, unless you run a Scheduled Pipeline for the first time.




### How to Use an External Container Registry

1. In [Settings > CI/CD > Variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    1. Set the [DOCKER_AUTH_CONFIG](https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#access-an-image-from-a-private-container-registry) variable that provides registry credentials. Note that we assume it's a `File` variable.
1. In [.gitlab-ci.yml](./.gitlab-ci.yml):
    1. Replace the `.gitlab-registry-login` anchor with an `.external-registry-login` appropriate to your `CONTAINER_BUILDER`.
    1. Replace the `$CI_REGISTRY_IMAGE` variable to your registry address in `APP_BUILDER_IMAGE` and `APP_SERVER_IMAGE` variables.

